/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlclient;
import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;
import java.sql.*;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ahrfry
 */
public class SQLExecuter {
    private Connection cn = null;
    private Statement stmt = null;
    private PreparedStatement ps = null;
    private String response = "Query executed successfully.";
    private String host = "127.0.0.1";
    private String user = "root";
    private String pass = "123243";
    //private String pass = "sirdante11";
    
    public SQLExecuter () throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        cn = DriverManager.getConnection(
                "jdbc:mysql://"+host+"/", user, pass);
    }
    
    public String Create(String query){
        
        try{
            stmt = cn.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            ex.printStackTrace();
            response = "There may be a problem with your SQL String. Please "
                    + "verify it and try again. See console for details.";
        }
        closeCon();
        return response;
    }
    
    public String Insert(String query){
        try{
            ps = cn.prepareStatement(query);
            int numRowsAffected = ps.executeUpdate();
            if (numRowsAffected >= 1) {
                response = "Values successfully inserted/updated/deleted."
                        + " Rows affected: " + numRowsAffected;
            } else {
                response = "Values were not successfully"
                        + " inserted/updated/deleted."
                        + " Please try again.";
            }
            System.out.println("finished try");
        } catch (SQLException ex) {
            ex.printStackTrace();
            response = "There may be a problem with your SQL String. Please "
                    + "verify it and try again. See console for details.";
        }
        
        closeCon();
        return response;
        
    }
    
    public DefaultTableModel Select(String query){
        String aux = ""; 
        String row = "";
        Vector<String> columnNames = null;
        Vector<Vector<Object>> data = null;
        try{
            
            ps = cn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();

            // Names of columns
            columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                columnNames.add(metaData.getColumnName(i));
            }

            // Data of the table
            data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++) {
                    vector.add(rs.getObject(i));
                }
                data.add(vector);
            }
            
           
           
        } catch (SQLException ex) {
            ex.printStackTrace();
            response = "There may be a problem with your SQL String. Please "
                    + "verify it and try again. See console for details.";
        }
        
        closeCon();
        return new DefaultTableModel(data, columnNames);
        
        
    }
    
    private String closeCon(){
        
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception se2) {
        }// nothing we can do
        
        try {
            if (cn != null) {
                cn.close();
            }
        } catch (Exception se) {
            se.printStackTrace();
        }
        
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (Exception se) {
            se.printStackTrace();
        }
        
        return response;
    }
  
}
